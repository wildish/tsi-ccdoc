Minikube and NGINX Practical
============================

Demo: Creating VMs with Terraform
---------------------------------

* Log onto `Embassy for ResOps <https://extcloud05.ebi.ac.uk>`_. You can see the overview of the tenancy, where no resops cluster is created.

.. image:: /static/images/resops2019/Horizon.Overview.png

* Inspect variable values at `https://gitlab.ebi.ac.uk/TSI/tsi-ccdoc/blob/latest/tsi-cc/ResOps/scripts/kubespray/resops.tf <https://gitlab.ebi.ac.uk/TSI/tsi-ccdoc/blob/latest/tsi-cc/ResOps/scripts/kubespray/resops.tf>`_. Make note of important variables such as `cluster_name`, `image`, `network_name`, `floatingip_pool`, `number_of_bastions`, `number_of_k8s_masters`, `number_of_k8s_nodes`, `number_of_k8s_nodes_no_floating_ip`, etc.. The describe the structure of the cluster.
* Inspect sample script `https://gitlab.ebi.ac.uk/TSI/tsi-ccdoc/blob/latest/tsi-cc/ResOps/scripts/kubespray/terraform.sh <https://gitlab.ebi.ac.uk/TSI/tsi-ccdoc/blob/latest/tsi-cc/ResOps/scripts/kubespray/terraform.sh>`_. Note that this script can not run other than on my laptop. It is shown to provide a rough idea how to run a Terraform script.
* Inspect Terraform script for OpenStack under `https://github.com/kubernetes-sigs/kubespray/tree/master/contrib/terraform/openstack <https://github.com/kubernetes-sigs/kubespray/tree/master/contrib/terraform/openstack>`_. This script is well-written, following a lot of best practices as well as carefully modulized.

  * Default values are provided in the top level `variables.tf`.
  * The infrastructure are divided into three modules: network, ips and compute in the top level `kubespray.tf`.
  * Each module is organized with `main.tf` (as required for a Terraform module), `variables.tf` (input) and `output.tf` (output). The resource description is in `main.tf`. The APIs can be found at `https://www.terraform.io/docs/providers/openstack/index.html <https://www.terraform.io/docs/providers/openstack/index.html>`_.

* `cd ~/IdeaProjects/tsi-ccdoc/tsi-cc/ResOps/` in a terminal window.
* Run `./terraform.sh` one line at a time. Explain details. In the end, resources are created according to `tsi-cc/ResOps/resops.tf`::

    Apply complete! Resources: 20 added, 0 changed, 0 destroyed.

    Outputs:

    bastion_fips = [
        193.62.55.21
    ]
    floating_network_id = e25c3173-bb5c-4bbc-83a7-f0551099c8cd
    k8s_master_fips = []
    k8s_node_fips = []
    private_subnet_id = e457fd0b-c02d-4287-b049-4f143a32b2fb
    router_id = 470c37d6-1ec6-49bd-9a9e-cc7712dfcf07

* Log back onto Embassy to see VMs (a bastion, 2 nodes without floating IPs), private network / subnet (resops), router (resops-1-router) are created.

.. image:: /static/images/resops2019/ResOpsVMs.png

* Access the VMs via SSH directly if they have public IPs attached. Otherwise, use SSH tunnel via bastion server, for example `ssh -i ~/.ssh/id_rsa -o UserKnownHostsFile=/dev/null -o ProxyCommand="ssh -W %h:%p -i ~/.ssh/id_rsa ubuntu@193.62.55.21" ubuntu@10.0.0.5`
* Add individual's public key to ~/.ssh/authorized_keys on the new VMs for workshop participants to use later.

NOTE: `-o UserKnownHostsFile=/dev/null` disables reading from and writing to ~/.ssh/known_hosts. This opens a security hole for man-in-the-middle attack. The option `-o StrictHostKeyChecking=No` would not work with `-o ProxyCommand` as the keys need to be exchanged. It is better practice for security to edit entries in ~/.ssh/known_hosts when deuplication happens but certainly less convenient.

Exercise 1: Adding Minikube to the new VMs
------------------------------------------

Kubernetes has comprehensive documentation on if and how to use Minikube `https://kubernetes.io/docs/setup/minikube/ <https://kubernetes.io/docs/setup/minikube/>`_.

Access the virtual workstation via bastion server, for example `ssh -i ~/.ssh/id_rsa -o UserKnownHostsFile=/dev/null -o ProxyCommand="ssh -W %h:%p -i ~/.ssh/id_rsa ubuntu@193.62.55.21" ubuntu@10.0.0.5`

The VM does not have virtualization of its own as expected. Neither Virtualbox or KVM works in this environment. The only option is to use Docker runtime::

  sudo apt-get update
  sudo apt install -y docker.io

The kubectl is needed as well::

  sudo snap install kubectl --classic

Download, install and configure Minikube::

  curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube
  sudo cp minikube /usr/local/bin && rm minikube

Finally start Minikube without a hypervisor::

  sudo minikube start --vm-driver=none

The following message should be displayed when Minikube is started successfully::

  😄  minikube v1.0.1 on linux (amd64)
  🔥  Creating none VM (CPUs=2, Memory=2048MB, Disk=20000MB) ...
  📶  "minikube" IP address is 10.0.0.5
  🐳  Configuring Docker as the container runtime ...
  🐳  Version of container runtime is 18.09.2
  ✨  Preparing Kubernetes environment ...
  ❌  Unable to load cached images: loading cached images: loading image /home/ubuntu/.minikube/cache/images/gcr.io/k8s-minikube/storage-provisioner_v1.8.1: stat /home/ubuntu/.minikube/cache/images/gcr.io/k8s-minikube/storage-provisioner_v1.8.1: no such file or directory
  💾  Downloading kubeadm v1.14.1
  💾  Downloading kubelet v1.14.1
  🚜  Pulling images required by Kubernetes v1.14.1 ...
  🚀  Launching Kubernetes v1.14.1 using kubeadm ...
  ⌛  Waiting for pods: apiserver proxy etcd scheduler controller dns
  🔑  Configuring cluster permissions ...
  🤔  Verifying component health .....
  🤹  Configuring local host environment ...

  ⚠️  The 'none' driver provides limited isolation and may reduce system security and reliability.
  ⚠️  For more information, see:
  👉  https://github.com/kubernetes/minikube/blob/master/docs/vmdriver-none.md

  ⚠️  kubectl and minikube configuration will be stored in /home/ubuntu
  ⚠️  To use kubectl or minikube commands as your own user, you may
  ⚠️  need to relocate them. For example, to overwrite your own settings:

      ▪ sudo mv /home/ubuntu/.kube /home/ubuntu/.minikube $HOME
      ▪ sudo chown -R $USER $HOME/.kube $HOME/.minikube

  💡  This can also be done automatically by setting the env var CHANGE_MINIKUBE_NONE_USER=true
  💗  kubectl is now configured to use "minikube"
  🏄  Done! Thank you for using minikube!

Note that /home/ubuntu/.kube/config is owned by root. Run `chown` to avoid using `sudo`::

  sudo chown -R $USER $HOME/.kube $HOME/.minikube

Exercise 2: Creating NGINX
--------------------------

Create a Kubernetes manifest file with `nano ~/nginx.yml`. You can copy and paste from `https://gitlab.ebi.ac.uk/TSI/tsi-ccdoc/blob/latest/tsi-cc/ResOps/scripts/minikube/nginx.yml <https://gitlab.ebi.ac.uk/TSI/tsi-ccdoc/blob/latest/tsi-cc/ResOps/scripts/minikube/nginx.yml>`_ to the editor. Review the file carefully to see what resources are to be created and how parts are connected with each other.

.. image:: /static/images/resops2019/nginx.yml.png

Apply the manifest to create a service and statefulset::

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl apply -f nginx.yml
  service/nginx created
  statefulset.apps/web created

This creates additional resources needed to for a cluster of NGINX servers(persistent volume claims, persistent volumes, pods, service)::

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl get pvc
  NAME        STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
  www-web-0   Bound    pvc-4cd5c31b-7718-11e9-8b0b-fa163ede6c1a   1Gi        RWO            standard       39s
  www-web-1   Bound    pvc-5ac9090b-7718-11e9-8b0b-fa163ede6c1a   1Gi        RWO            standard       16s

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl get pv
  NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM               STORAGECLASS   REASON   AGE
  pvc-4cd5c31b-7718-11e9-8b0b-fa163ede6c1a   1Gi        RWO            Delete           Bound    default/www-web-0   standard                41s
  pvc-5ac9090b-7718-11e9-8b0b-fa163ede6c1a   1Gi        RWO            Delete           Bound    default/www-web-1   standard                18s

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl get pod
  NAME    READY   STATUS    RESTARTS   AGE
  web-0   1/1     Running   0          52s
  web-1   1/1     Running   0          29s

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl get svc
  NAME         TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
  kubernetes   ClusterIP   10.96.0.1        <none>        443/TCP   61m
  nginx        ClusterIP   10.104.206.229   <none>        80/TCP    76s

Exercise 3: Hello World
-----------------------

Each of the two NGINX server has its own storage, running in its own pod but share the same cluster IP `10.104.206.229`. If you try to access the home page, you will get HTTP403::

  ubuntu@resops-1-k8s-node-nf-1:~$ curl http://10.104.206.229
  <html>
  <head><title>403 Forbidden</title></head>
  <body>
  <center><h1>403 Forbidden</h1></center>
  <hr><center>nginx/1.15.12</center>
  </body>
  </html>

Let's fix this on one of the two NGINX servers. Recall that the volume `www` is mounted on `/usr/share/nginx/html/`, which is the default document root for NGINX server. Connect to pod web-0 and you can see that there is no file to be served by NGINX. That's why the HTTP403 is sent back::

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl exec -it web-0 -- /bin/bash
  root@web-0:/# cd /usr/share/nginx/html/
  root@web-0:/usr/share/nginx/html# ls -la
  total 8
  drwxrwxrwx 2 root root 4096 May 15 13:49 .
  drwxr-xr-x 3 root root 4096 May  8 03:01 ..

Create a simple `/usr/share/nginx/html/index.html` with a title and heading "Hello World from web-0"::

  cat <<EOF > /usr/share/nginx/html/index.html
  <html>
  <head><title>Hello World from web-0</title></head>
  <body>
  <center><h1>Hello World from web-0</h1></center>
  <hr><center>nginx/1.15.12</center>
  </body>
  </html>
  EOF

Exit out of the pod web-0::

  root@web-0:/usr/share/nginx/html# exit
  exit

Repeat the process in pod web-1. It is important to use a title and heading "Hello World from web-1" so that the two pods have two different index.html files::

  kubectl exec -it web-1 -- /bin/bash

  cat <<EOF > /usr/share/nginx/html/index.html
  <html>
  <head><title>Hello World from web-1</title></head>
  <body>
  <center><h1>Hello World from web-1</h1></center>
  <hr><center>nginx/1.15.12</center>
  </body>
  </html>
  EOF

Exit out of the pod web-1. Send HTTP request to the cluster IP again. You will find that the two NGINX servers take turns to serve the home page::

  ubuntu@resops-1-k8s-node-nf-1:~$ curl http://10.104.206.229
  <html>
  <head><title>Hello World from web-1</title></head>
  <body>
  <center><h1>Hello World from web-1</h1></center>
  <hr><center>nginx/1.15.12</center>
  </body>
  </html>

  ubuntu@resops-1-k8s-node-nf-1:~$ curl http://10.104.206.229
  <html>
  <head><title>Hello World from web-0</title></head>
  <body>
  <center><h1>Hello World from web-0</h1></center>
  <hr><center>nginx/1.15.12</center>
  </body>
  </html>

Exercise 4: Persistence and disaster recovery
---------------------------------------------

From exercises 2 & 3, we understand that the NGINX serves independent copies from `/usr/share/nginx/html` persisted on two separate volumes via two pods web-0 and web-1. See output by `kubectl get` commands in exercise 2 and `curl http://10.104.206.229` command in exercise 3 again.

If a stateful set is removed, the persistent volumes are preserved by design. Run `kubectl delete -f nginx.yml` to simulate scheduled outage. The service and pods are deleted but persistent volumes are saved::

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl delete -f nginx.yml
  service "nginx" deleted
  statefulset.apps "web" deleted

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl get pvc
  NAME        STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
  www-web-0   Bound    pvc-4cd5c31b-7718-11e9-8b0b-fa163ede6c1a   1Gi        RWO            standard       110m
  www-web-1   Bound    pvc-5ac9090b-7718-11e9-8b0b-fa163ede6c1a   1Gi        RWO            standard       110m

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl get pv
  NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM               STORAGECLASS   REASON   AGE
  pvc-4cd5c31b-7718-11e9-8b0b-fa163ede6c1a   1Gi        RWO            Delete           Bound    default/www-web-0   standard                110m
  pvc-5ac9090b-7718-11e9-8b0b-fa163ede6c1a   1Gi        RWO            Delete           Bound    default/www-web-1   standard                110m

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl get svc
  NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
  kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   173m

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl get pod
  No resources found.

Run `kubectl apply -f nginx.yml` to recreate the stateful set and service. The each new pod recreated will mount its original volume as if it was never deleted. The pod web-0 still has the index.html with the message of "Hello World from web-0"::

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl apply -f nginx.yml
  service/nginx created
  statefulset.apps/web created

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl get pod
  NAME    READY   STATUS    RESTARTS   AGE
  web-0   1/1     Running   0          14s
  web-1   1/1     Running   0          7s

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl exec -it web-0 -- /bin/bash
  root@web-0:/# cat /usr/share/nginx/html/index.html
  <html>
  <head><title>Hello World from web-0</title></head>
  <body>
  <center><h1>Hello World from web-0</h1></center>
  <hr><center>nginx/1.15.12</center>
  </body>
  </html>

Run `kubectl delete pod web-1` to simlate unscheduled outage. The recovery happens really fast. You need to send the following commands really quickly::

  kubectl delete pod web-1
  kubectl get pod

Kubernete tries to restart pod web-1 immediately. After a little while web-1 will be running again as if noting happened. It will be mounted to its original volume. The index.html in used by pod web-1 still has the same message of "Hello World from web-1"::

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl delete pod web-1
  pod "web-1" deleted

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl get pod
  NAME    READY   STATUS              RESTARTS   AGE
  web-0   1/1     Running             0          6m50s
  web-1   0/1     ContainerCreating   0          0s

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl get pod
  NAME    READY   STATUS    RESTARTS   AGE
  web-0   1/1     Running   0          11m
  web-1   1/1     Running   0          4m27s

  ubuntu@resops-1-k8s-node-nf-1:~$ kubectl exec -it web-1 -- /bin/bash
  root@web-1:/# cat /usr/share/nginx/html/index.html
  <html>
  <head><title>Hello World from web-1</title></head>
  <body>
  <center><h1>Hello World from web-1</h1></center>
  <hr><center>nginx/1.15.12</center>
  </body>
  </html>

