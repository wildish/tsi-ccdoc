EMBL-EBI Cloud Portal documentation
===================================

* `EMBL-EBI Cloud Portal documentation <https://ebi-cloud-portal.readthedocs.io/en/latest/>`_

  * `Using the EMBL-EBI Cloud Portal <https://ebi-cloud-portal.readthedocs.io/en/latest/using_the_portal.html>`_
  * `Packaging Applications for the EMBL-EBI Cloud Portal <https://ebi-cloud-portal.readthedocs.io/en/latest/applications_packaging.html>`_
  * `Avoid security credentials on git public repository <https://ebi-cloud-portal.readthedocs.io/en/latest/securing_credentials.html>`_
  * `API Endpoint documentation <https://ebi-cloud-portal.readthedocs.io/en/latest/api_endpoints.html>`_

* Presentations on UKRI 2019

  * `EMBL-EBI Cloud Portal for Everyone <../_static/pdf/ukri2019/EMBL-EBICloudPortalforEveryone.pdf>`_
  * `Case Study of Porting Rfam Pipeline into Cloud <../_static/pdf/ukri2019/CaseStudyofPortingRfamPipelineintoCloud.pdf>`_

.. * `Porting RFAM to Cloud Portal <https://drive.google.com/open?id=1NWtV0NlBErY_5ueDnJp60m0nsrkIgtKf>`_