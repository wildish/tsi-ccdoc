Arvados on Kubernetes
=====================

Arvados installation options
----------------------------

Arvados on Kubernetes is under development. Here is a link to the `installation options <https://doc.arvados.org/install/index.html>`_.

The official documentation on how to install Arvados on Kubernetes can be found `here <https://doc.arvados.org/install/arvados-on-kubernetes.html>`_.

Kubenetes cluster
-----------------

There are three options to host a Kubernete cluster:

#. CaaS at EBI. The Helm chart requires the namespace of `kube-system`, which TSI has no access to.
#. GKE on GCP. This is a good option for production. This requires funding.
#. Kubernetes CPA on ECP backed by OSK.

It is also possible to run it on Kubernetes clusters in AWS or MSA. The best option is GKE. The vendor only tested their Helm chart in GKE.

Kubernetes cluster on GKE
-------------------------

Run the following command in Cloud Shell::

  gcloud beta container --project "extreme-lore-114513" clusters create "hdr-uk-1" --region "europe-north1" --no-enable-basic-auth --cluster-version "1.11.6-gke.2" --machine-type "n1-standard-2" --image-type "COS" --disk-type "pd-standard" --disk-size "100" --scopes "https://www.googleapis.com/auth/cloud-platform" --num-nodes "3" --enable-cloud-logging --enable-cloud-monitoring --no-enable-ip-alias --network "projects/extreme-lore-114513/global/networks/default" --subnetwork "projects/extreme-lore-114513/regions/europe-north1/subnetworks/default" --enable-autoscaling --min-nodes "3" --max-nodes "20" --addons HorizontalPodAutoscaling,HttpLoadBalancing --enable-autoupgrade --enable-autorepair

  davidyuan@cloudshell:~ (extreme-lore-114513)$ gcloud beta container --project "extreme-lore-114513" clusters create "hdr-uk-1" --region "europe-north1" --no-enable-basic-auth --cluster-version "1.11.6-gke.2" --machine-type "n1-standard-2" --image-type "COS" --disk-type "pd-standard" --disk-size "100" --scopes "https://www.googleapis.com/auth/cloud-platform" --num-nodes "3" --enable-cloud-logging --enable-cloud-monitoring --no-enable-ip-alias --network "projects/extreme-lore-114513/global/networks/default" --subnetwork "projects/extreme-lore-114513/regions/europe-north1/subnetworks/default" --enable-autoscaling --min-nodes "3" --max-nodes "20" --addons HorizontalPodAutoscaling,HttpLoadBalancing --enable-autoupgrade --enable-autorepair
  WARNING: Starting in 1.12, new clusters will not have a client certificate issued. You can manually enable (or disable) the issuance of the client certificate using the `--[no-]issue-client-certificate` flag.
  WARNING: Starting in 1.12, default node pools in new clusters will have their legacy Compute Engine instance metadata endpoints disabled by default. To create a cluster with legacy instance metadata endpoints disabled in the default node pool, run `clusters create` with the flag `--metadata disable-legacy-endpoints=true`.
  This will enable the autorepair feature for nodes. Please see https://cloud.google.com/kubernetes-engine/docs/node-auto-repair for more information on node autorepairs.
  This will enable the autoupgrade feature for nodes. Please see https://cloud.google.com/kubernetes-engine/docs/node-management for more information on node autoupgrades.
  Creating cluster hdr-uk-1 in europe-north1... Cluster is being health-checked (master is healthy)...done.
  Created [https://container.googleapis.com/v1beta1/projects/extreme-lore-114513/zones/europe-north1/clusters/hdr-uk-1].
  To inspect the contents of your cluster, go to: https://console.cloud.google.com/kubernetes/workload_/gcloud/europe-north1/hdr-uk-1?project=extreme-lore-114513
  kubeconfig entry generated for hdr-uk-1.
  NAME      LOCATION       MASTER_VERSION  MASTER_IP       MACHINE_TYPE   NODE_VERSION  NUM_NODES  STATUS
  hdr-uk-1  europe-north1  1.11.6-gke.2    35.228.159.196  n1-standard-2  1.11.6-gke.2  9          RUNNING

Configure kubectl command-line access::

  gcloud beta container clusters get-credentials hdr-uk-1 --region europe-north1 --project extreme-lore-114513

Note that the default namespace is not used. Pods are under kube-system namespace.

Helm installation
-----------------

Install Helm and Tiller in the namespace of kube-system on Kubernetes master::

  curl https://raw.githubusercontent.com/helm/helm/master/scripts/get | bash
  helm init --upgrade
  kubectl create serviceaccount --namespace kube-system tiller
  kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
  kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'

Clone Git repository
-----------------------

Install Git client before cloning the repository::

  sudo yum install git
  git clone https://github.com/curoverse/arvados-kubernetes.git
  cd arvados-kubernetes/charts/arvados

Start a cluster
---------------

Reserve a static IP at `VPC network <https://console.cloud.google.com/networking/addresses/list?project=extreme-lore-114513>`_. Here are the `detailed instructions <https://cloud.google.com/compute/docs/ip-addresses/reserve-static-external-ip-address>`_::

  ./cert-gen.sh 35.228.126.19
  Generating a RSA private key
  .................+++++
  ..................+++++
  writing new private key to './config/ssl/key'
  -----

Update values.yml as needed, for example::

  adminUserEmail
  adminUserPassword
  superUserSecret
  anonymousUserSecret
  nodes

Start the Arvados cluster::

  helm install --name arvados . --set externalIP=35.228.126.19

Check the status with `kubectl`::

  kubectl get pods
  kubectl get svc

Test the web access at::

  https://35.228.126.19

Final note
----------

There seems significant mismatch what is deployed by the Helm chart and what features are available via GUI (e.g. https://35.228.126.19) according to the documentation. Perhaps, Some work is needed considering the chart was not really updated alone with the runtime.
