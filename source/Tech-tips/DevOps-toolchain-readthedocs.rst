DevOps toolchain from IntelliJ to ReadtheDocs for publishing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The toolchain for documentation consists of the following tools:

#. `IntelliJ IDEA CE <https://tsi-ccdoc.readthedocs.io/en/latest/Tech-tips/Cloud-Consulting-Team-toolbox.html#intellij-idea>`_
#. `GitLab <https://gitlab.ebi.ac.uk/TSI/tsi-ccdoc/>`_
#. `Read the Docs <https://readthedocs.org/projects/tsi-ccdoc/>`_

The source is stored in GitLab. Follow the instructions to install `IntelliJ IDEA CE <https://tsi-ccdoc.readthedocs.io/en/latest/Tech-tips/Cloud-Consulting-Team-toolbox.html#intellij-idea>`_. Check out the project from source control repository. The easiest is to clone the project via HTTPS.

After `*.md` or `*.rst` files are added or edited, a `Personal Access Token <https://gitlab.ebi.ac.uk/profile/personal_access_tokens>`_ is needed to push the changes instead of a regular password. This is because the 2FA authentication is enabled. Please make sure that the token has api scope and never expires. Make sure IntelliJ remembers the token / password so that it can be reused for pushes in the future.

The ReadtheDocs project is configured to pull the respsitory from GitLab. `Build <https://readthedocs.org/projects/tsi-ccdoc/builds/>`_ can be invoked by clicking "Build Version:". It is usually the `latest` version. It is possible to configure Read the Docs to trigger a build with every push. However, it makes little sense.

Local build with Sphinx
-----------------------

To create a local build environment, install `sphinx` with `pip`::

  C02XD1G9JGH7:tsi-ccdoc davidyuan$ sudo pip install sphinx

Initialize the project with `sphinx-quickstart`. Make sure the `rst` is selected::

  C02XD1G9JGH7:tsi-ccdoc davidyuan$ sphinx-quickstart
  Welcome to the Sphinx 1.8.2 quickstart utility.

  Please enter values for the following settings (just press Enter to
  accept a default value, if one is given in brackets).

  Selected root path: .

  You have two options for placing the build directory for Sphinx output.
  Either, you use a directory "_build" within the root path, or you separate
  "source" and "build" directories within the root path.
  > Separate source and build directories (y/n) [n]: y

  Inside the root directory, two more directories will be created; "_templates"
  for custom HTML templates and "_static" for custom stylesheets and other static
  files. You can enter another prefix (such as ".") to replace the underscore.
  > Name prefix for templates and static dir [_]:

  The project name will occur in several places in the built documentation.
  > Project name: tsi-cc
  > Author name(s): David Yuan
  > Project release []: 1.0

  If the documents are to be written in a language other than English,
  you can select a language here by its language code. Sphinx will then
  translate text that it generates into that language.

  For a list of supported codes, see
  http://sphinx-doc.org/config.html#confval-language.
  > Project language [en]:

  The file name suffix for source files. Commonly, this is either ".txt"
  or ".rst".  Only files with this suffix are considered documents.
  > Source file suffix [.rst]:

  One document is special in that it is considered the top node of the
  "contents tree", that is, it is the root of the hierarchical structure
  of the documents. Normally, this is "index", but if your "index"
  document is a custom template, you can also set this to another filename.
  > Name of your master document (without suffix) [index]:
  Indicate which of the following Sphinx extensions should be enabled:
  > autodoc: automatically insert docstrings from modules (y/n) [n]:
  > doctest: automatically test code snippets in doctest blocks (y/n) [n]:
  > intersphinx: link between Sphinx documentation of different projects (y/n) [n]:
  > todo: write "todo" entries that can be shown or hidden on build (y/n) [n]:
  > coverage: checks for documentation coverage (y/n) [n]:
  > imgmath: include math, rendered as PNG or SVG images (y/n) [n]:
  > mathjax: include math, rendered in the browser by MathJax (y/n) [n]:
  > ifconfig: conditional inclusion of content based on config values (y/n) [n]:
  > viewcode: include links to the source code of documented Python objects (y/n) [n]:
  > githubpages: create .nojekyll file to publish the document on GitHub pages (y/n) [n]:

  A Makefile and a Windows command file can be generated for you so that you
  only have to run e.g. `make html' instead of invoking sphinx-build
  directly.
  > Create Makefile? (y/n) [y]:
  > Create Windows command file? (y/n) [y]: n

  Creating file ./source/conf.py.
  Creating file ./source/index.rst.
  Creating file ./Makefile.

  Finished: An initial directory structure has been created.

  You should now populate your master file ./source/index.rst and create other documentation
  source files. Use the Makefile to build the docs, like so:
     b.make builder
  where "builder" is one of the supported builders, e.g. html, latex or linkcheck.

now, a local test build can be created::

  c02xd1g9jgh7:tsi-ccdoc davidyuan$ make html

the index.html can be opened to see a skeleton project documentation::

  c02xd1g9jgh7:tsi-ccdoc davidyuan$ open build/html/index.html

Using markdown with sphinx
--------------------------

the restructuredtext is powerful but its syntax is also a bit overwhelming. the markdown can be a alternative for simple documentation. the 'recommonmark' is needed to use markdown and restructuredtext in the same sphinx project.::

  c02xd1g9jgh7:tsi-ccdoc davidyuan$ sudo pip install recommonmark

then, `conf.py` would also needs to be updated.::

  from recommonmark.parser import commonmarkparser

  source_parsers = {
    '.md': commonmarkparser,
  }

  source_suffix = ['.rst', '.md']

Reference
---------

#. `Restructured text primer <http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_
#. `Getting started with sphinx <https://docs.readthedocs.io/en/latest/intro/getting-started-with-sphinx.html>`_
#. `Markdown cheat sheet <https://www.markdownguide.org/cheat-sheet/>`_
