# Installing OpenStack CLI on Mac OS X
## Prerequisite
Python 2.7 and setuptools are installed by default. This can be verified --version option on them.

```
C02XD1G9JGH7:~ davidyuan$ python --version
Python 2.7.10
C02XD1G9JGH7:~ davidyuan$ easy_install --version
setuptools 18.5 from /System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python (Python 2.7)
C02XD1G9JGH7:~ davidyuan$ 
```

## Installation
Install pip 18.0 for Python 2.7.10 as root.

`# easy_install pip`

The package tornado is required by openstack client. It can be installed with pip as root first if not installed already. Otherwise, error message “matplotlib 1.3.1 requires nose, which is not installed. matplotlib 1.3.1 requires tornado, which is not installed.” might be seen.

`# pip install tornado`

The installation of python-openstackclient may fail with the error of “Cannot uninstall 'six'. It is a distutils installed project and thus we cannot accurately determine which files belong to it which would lead to only a partial uninstall.” User options --ignore-installed six --user to bypass it.

`# pip install python-openstackclient --ignore-installed six --user`

## Configuration
Clients are installed under “/Users/davidyuan/Library/Python/2.7/bin“. It is easier to add the directory to PATH.

`C02XD1G9JGH7:bin davidyuan$ sudo vi /etc/paths`

Connect to Horizon to download RC files under API access at https://extcloud05.ebi.ac.uk/dashboard/project/access_and_security/. Place the file EBI-TSI-DEV-openrc.sh under a working directory (e.g. /Users/davidyuan/). Make it executable.

`chmod a+x EBI-TSI-DEV-openrc.sh`

## Verification
```
C02XD1G9JGH7:~ davidyuan$ ./EBI-TSI-DEV-openrc.sh
Please enter your OpenStack Password: 
C02XD1G9JGH7:~ davidyuan$ nova list
+--------------------------------------+-------------------------+---------+------------+-------------+---------------------------------------------------+
| ID                                   | Name                    | Status  | Task State | Power State | Networks                                          |
+--------------------------------------+-------------------------+---------+------------+-------------+---------------------------------------------------+
| d84f4480-7668-45d5-b865-2c15e5be3f27 | bastion                 | SHUTOFF | -          | Shutdown    | test_network=192.168.0.45                         |
| fbd97878-cdeb-40a6-80ba-34ce05c93e6e | dsds-test               | ACTIVE  | -          | Running     | test_network=192.168.0.48                         |
| 473a240d-ed73-416a-a981-41c7cb856fed | gridftp1                | SHUTOFF | -          | Shutdown    | test_network=192.168.0.41, 193.62.52.98           |
| 5b1b9d6a-ebaa-48bc-a241-786230eee487 | lsf-master              | SHUTOFF | -          | Shutdown    | test_network=192.168.0.18                         |
| 65430b10-16f2-490a-8b3a-3f20064c2ed6 | lsf-node1               | SHUTOFF | -          | Shutdown    | test_network=192.168.0.15                         |
| c7ddf10a-36c7-4c01-9f0f-673d8f42b575 | lsf-node10              | SHUTOFF | -          | Shutdown    | test_network=192.168.0.24                         |
| 9976bd27-6d09-4561-a278-0a43e221a3a1 | lsf-node11              | SHUTOFF | -          | Shutdown    | test_network=192.168.0.20                         |
| 8ad71500-27a5-4895-ae18-1b82dac1a864 | lsf-node12              | SHUTOFF | -          | Shutdown    | test_network=192.168.0.21                         |
| 6b9812ab-6f0d-414b-89a7-fc408a2c05ad | lsf-node13              | SHUTOFF | -          | Shutdown    | test_network=192.168.0.22                         |
| 795c57c6-1695-4d1b-bda2-b87788830106 | lsf-node2               | SHUTOFF | -          | Shutdown    | test_network=192.168.0.25                         |
| 06f122d0-25b1-4a65-aa52-4421f7309b98 | lsf-node3               | SHUTOFF | -          | Shutdown    | test_network=192.168.0.26                         |
| 6633ae23-227c-43ac-b1ec-a3adf887db23 | lsf-node4               | SHUTOFF | -          | Shutdown    | test_network=192.168.0.16                         |
| 7fe0532a-4b00-4069-b47e-335bef1c727d | lsf-node5               | SHUTOFF | -          | Shutdown    | test_network=192.168.0.17                         |
| a0cf962f-04a5-4052-a3eb-bbd211ed8907 | lsf-node6               | ACTIVE  | -          | Running     | test_network=192.168.0.12                         |
| 246ebd68-be1e-4a28-8cd8-d7f780b77c8a | lsf-node7               | SHUTOFF | -          | Shutdown    | test_network=192.168.0.13                         |
| bfc75e2a-b6d5-4abf-ac35-eee511398aed | lsf-node8               | SHUTOFF | -          | Shutdown    | test_network=192.168.0.14                         |
| c154aafb-715e-42da-9e15-0b828c855d17 | lsf-node9               | SHUTOFF | -          | Shutdown    | test_network=192.168.0.23                         |
| c9f8192c-a0ec-40e1-b15e-e40e1a37aec8 | nfs-server-1            | SHUTOFF | -          | Shutdown    | test_network=192.168.0.34, 193.62.52.67           |
| 00fb3ad6-a57f-458e-9b99-1437f2041f1d | pymol-test              | SHUTOFF | -          | Shutdown    | test_network=192.168.0.37, 193.62.52.122          |
| b32bb42b-79b3-4cc9-adbb-114e30d0c163 | tesk-k8s-prod-master    | ACTIVE  | -          | Running     | tesk-k8s-prod-network=10.0.0.5, 193.62.55.68      |
| a1c83dae-b244-4683-855e-56c6de5c79b9 | tesk-k8s-prod-node-0    | ACTIVE  | -          | Running     | tesk-k8s-prod-network=10.0.0.9                    |
| 6a881aea-701a-447e-90cc-855d6ffa31e0 | tesk-k8s-prod-node-1    | ACTIVE  | -          | Running     | tesk-k8s-prod-network=10.0.0.10                   |
| 6f2857b8-80c6-4623-9328-df57975ac7f9 | tesk-k8s-prod-node-2    | ACTIVE  | -          | Running     | tesk-k8s-prod-network=10.0.0.6                    |
| 3094c4e6-2d99-43cd-8452-27e56a464edf | tesk-k8s-prod-node-3    | ACTIVE  | -          | Running     | tesk-k8s-prod-network=10.0.0.7                    |
| e783e094-e781-4908-8d8f-906bba472c4e | tesk-k8s-prod-node-4    | ACTIVE  | -          | Running     | tesk-k8s-prod-network=10.0.0.8                    |
| 75fd4c5a-4b6b-4062-ae3a-5bacba7bf6f9 | tesk-k8s-testing-master | ACTIVE  | -          | Running     | tesk-k8s-testing-network=10.0.0.105, 193.62.55.65 |
| 26f576f9-f053-48e5-a9e4-f24555be514f | tesk-k8s-testing-node-0 | ACTIVE  | -          | Running     | tesk-k8s-testing-network=10.0.0.104               |
| 8e7b66ec-5f47-41af-bad0-8c6a5b35155d | tesk-k8s-testing-node-1 | ACTIVE  | -          | Running     | tesk-k8s-testing-network=10.0.0.103               |
| cf1d3824-1654-4e2e-b3a7-61281c7e75d2 | test_comps              | SHUTOFF | -          | Shutdown    | test_network=192.168.0.11, 193.62.52.84           |
| 41e000ca-7369-415e-b64a-a2640c6752ed | tsi1535451150700-1      | SHUTOFF | -          | Shutdown    | test_network=192.168.0.44, 193.62.52.105          |
+--------------------------------------+-------------------------+---------+------------+-------------+---------------------------------------------------+
```

## Configuration
### Import or export key
OpenStack images tend to have password logon disabled for security. You can either import a public key into OpenStack or export a private key from OpenStack under Access & Security” > “Key Pairs”.
### Import public key
It is easier and safer to take this approach. Perform the following on the SSH client to generate a RSA key.

`ssh-keygen -t rsa -f cloud.key`

Copy and paste the content of cloud.key after clicking “Import Key Pair”
### Export private key
For multiple SSH clients and SFTP clients, the better options is to export the private key from OpenStack.
Click “Create Key Pair” and save the private key locally.
Change permission to 600 to secure the key immediately.

`chmod 600 ./id_rsa`

Move the key to the default location.

`mv ./id_rsa ~/.ssh`

Store the key in the keychain.

`ssh-add -K ~/.ssh/id_rsa`

### Generate public key from a private key
It is impossible to generate a private key from a public key on most servers due to limited computing power. However, it is easy and can be handy to keep the matching public key around by running.

`ssh-keygen -y -f ./id_rsa > ./id_rsa.pub`

## Reference
https://pypi.org/project/python-openstackclient/ 
