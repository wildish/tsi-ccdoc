# GitLab CI: Some best practices  

> **Read the docs.** read them again. [GitLab CI/CD](https://docs.gitlab.com/ce/ci/yaml/)

documentation of this is a great resource of setup.
> **Use GitLab CI Multi Runner.**

You can use fetch to make it faster, that’s ok. we recommend running your own CI runner VMs, with a shell runner inside, if you have resources for that.
> **Use stages.**

You want stages for many reasons. One is so you can see WHERE In the build it failed. Did it fail in compile_dependencies or did it fail in unit_tests or did it fail in integration_tests, or main_app_build, or did it fail at deploy?
> **Add tasks, and assign them stages. Set what they depend on.**

Things which can be done in parallel, make part of same stage and not depend on each other. Imagine you want to run “test_suite_a” and “test_suite_b” in parallel, make them separate tasks in the “testing” stage.
> **Use the “allow_failure” flag for stages that might fail for non-code-errors.**

Deployment can fail due to network issue or credentials or something else.
> **Must use Artifacting**

Artifacting to pass outputs of one stage to another. Also use artifacting to let you download each build as a zip, and maybe manually deploy it somewhere for testing, or just to “look at yesterday’s build outputs and compare them to todays”.
> **Use Caching**

use caching to speed up repeated builds on languages like C and C++ that generate a lot of .obj files and where their compilers are faster the second time, if you cache the intermediate files. Caching does not pass from one stage to another unless you define both caching and artifacting.
> **No stages which last >= 30min, Rule of thumb**

Don’t make stages that last more than 30 minutes.
> **Split massive tests into multiple tasks**

When you have a lot of tests, split your test task into multiple tasks and let them run in parallel.
> **Production deployment must be a manual task.**

maybe make production deployment a task, which is Manual? maybe make review/integrations deployment automatic, to a different server?
