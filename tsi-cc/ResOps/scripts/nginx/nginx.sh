#!/usr/bin/env bash

kubectl apply -f source/static/scripts/nginx/web.yml

kubectl get service nginx
kubectl get statefulset web
kubectl get pods -w -l app=nginx

kubectl apply -f source/static/scripts/nginx/ingress.yml