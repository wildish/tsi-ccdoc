## Requirements and status 2018.10.26

Hi David,

Thank you very much for looking into this. I really appreciate all the effort you put into this project. 
I took some time to go through all the points and try think and put all of these into K8s concepts.

Please find my comments in line.

Kind regards,
Ioanna

Ioanna  Kalvari
Software Developer - Rfam
European Bioinformatics Institute (EMBL-EBI)

Tel:	+ 44 (0) 1223 494279
Fax:	+ 44 (0) 1223 494468
Email: ikalvari@ebi.ac.uk


On Oct 25, 2018, at 2:08 PM, David Yuan <davidyuan@ebi.ac.uk> wrote:

Hi Ioanna,

Thank you for creating the image. I have done experiments with it. The results and conclusions are recorded in the section of “2018/10/24” on https://gitlab.ebi.ac.uk/davidyuan/tsi-ccdoc/wikis/rfam-cloud/rfam-tmp-results.

I would like to summarise the findings for your convenience.

1. The nonroot user centos was created correctly with the same UID, GID and name as the user on the host. It is extremely important to keep the same UID and GID for proper file ownership in the container and on the host.
2. Volumes from both NSF and local disk can be mounted to the container for read and write with any arbitrary path.
   1. NFS volume is accessed as nobody:nogroup in the container, and nfsnobody:nfsnobody on the host.
   2. Local disk such as /home/centos can be read from and written to as centos:centos both inside and outside.
3. As centos in the container is created with password disabled, it is unable to run 'su -‘. In addition, ‘sudo' is unavailable in the container. There is no way for the nonroot user to elevate its privilege, which secures all the processes in the container.

Up to this point we are in agreement. I’ve tested the image myself before making it available on docker hub. Many thanks for the suggestion of the of the non-root user. 

4. To provide secure persistent storage to multiple users, we can introduce a concept of vault token. We can design the token to be a long string beyond human comprehension. There are many algorithms to make such string unique, valid as UNIX path name, and with low probability of collision, such as hash of a public key.
   1. You assign a unique vault token to a new user. Keep a record which token is assigned to who in a table, which can only be viewed by an administrator.

This is brilliant! Thank you very much! 

   2. You give user access to Cloud Portal where they can launch K8S cluster for RFAM. This part has been taken care of by SSO for the portal.

Cloud Portal is indeed a very good solution to easily provision mini Rfam clusters to the users. You are absolutely right that we need to think of resource consumption when using Cloud resources and this will require minimizing the time a user has resources allocated to reduce cost too. I’m still not 100% convinced about the Cloud Portal, I’m close enough though. I need to investigate a little bit more and perhaps arrange a meeting to look into it together, but I would like to put this aside for now. 

For the prototype we want to have a “permanent” k8s cluster running in my Embassy tenancy and give access to the users to that instead. Something like the LSF cluster we have here on campus, so we’ll introduce user isolated k8s clusters and the Cloud Portal at a later point. We need to keep those in mind though, so thanks for looking into this.

I already modified KubeNow to allow multiple master nodes and a public ip for my glusterFS node, so I’ll stick to this for now.

   3. The user creates K8S cluster with their unique vault token. If this is the first time that the token is seen by the system, a new empty vault is created by the system. Otherwise, an existing vault is opened where the data stored by the token owner can be read and written again.
   4. When the user exits, the container itself is destroyed to release computing resources. The user data is persisted in the vault.

The implementation for such vault can be very simple. It is just a subdirectory on the persistent volume with NFS facade for example. The implementation detail can be completely hidden from a user using the container.


Regarding points 3 & 4, If we’ll be using a database to store the vault tokens, we can also keep track of a user’s activity status (i.e. active/inactive). This will come in handy upon logout, which in such an event would be changing the status in the database to “inactive” and automatically trigger the destruction of the pods or the cluster (when we get to that point). Food for thought. Better ideas are always welcome. That is the purpose of this collaboration anyway.

In your Ansible script, perform the following when launching the container:

```
# Create a new subdirectory if does not exist, and do nothing if exists.
mkdir /p /mnt/nfs/<vault_token>

# Wrap this in a script
docker run -it --mount type=bind,source=/mnt/nfs/<vault_token>,target=/app/pvol ikalvari/rfam-cloud:user-test
```

I would add this command in the user’s `.bashrc` file, so that when they ssh to an edge node, they automatically login to the container. This can be done by injecting this command into the .bashrc file when we build the image. I would also change the permissions to `.bashrc` to read only, along with restricting permissions on commands we don’t want them to have access to.  

Regarding the docker containers, I feel the need to point out that when it comes to K8s the concept is slightly different. K8s clusters allow one to use the docker cli to create containers by default, but when it comes to using the actual cluster, other concepts come into play. K8s cluster comes with it’s own cli, kubectl, and one would have to use that one in order to run jobs against the cluster. So an equivalent to your docker run command would look like:

`kubectl run USER_VAULT_TOKEN -it --image=ikalvari/rfam-cloud:user-test`

Where USER_VAULT_TOKEN will correspond to the deployment and pod names

A pod is a wrapper to a docker container that also encapsulates other information too, such as ENV variables, and mounts required to run the docker container. 

I found this very useful when started working with K8s, perhaps you will find it useful too:
https://kubernetes.io/docs/reference/kubectl/docker-cli-to-kubectl/

For now, let’s build up the Rfam cluster around this scenario: 
- The Rfam k8s cluster is constantly available 
- We handle resource consumption at K8s level, meaning that we allocate resources to a user via pod deployment. The idea is that with a user login, we provide the user with an environment to run the pipeline, and whenever necessary we allocate resources (again in the form of pods) to do some processing. For this purpose let me introduce the following 2 concepts: 
  - login/init pod: an interactive pod giving the users access to an interactive container, from where they can run the pipeline scripts. See this as an equivalent to a an interactive bsub session  you would request by running `bsub -Is $SHELL`
  - worker pods: pods launched directly from the pipeline scripts to do some data processing. See this as an equivalent to the standard `bsub` command
- Allow a user’s pods to run to completion and then delete them to releasing the resources. This means that even if a user logs out, the pods will run to completion and the users will be able to access the results when they log back in. We’ll need to introduce a limit on this one, so very correctly pointed out we’ll need to make use of a database.

Neither the user nor your pipeline would know that they are reading or persisting user data in a unique subdirectory in the container. It is the same logical target /app/pvol that they are dealing with. Different users would not be aware of other’s vault although they always use the same UNIX user (e.g. centos).

This is super useful and a very neat solution, thank you very much! 

Please feel free to let me know about any thoughts or concerns and things that we need to aware of. Better ideas are always welcome!

Many thanks!