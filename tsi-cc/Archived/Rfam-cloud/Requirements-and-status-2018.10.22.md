## Requirements and status 2018.10.22

Hi Ioanna, Tony, Gianni and CD,

Please see minutes based on my faulty memory. Please do not hesitate to suggest any changes.

Best regards,

David

Attendees:
	Ioanna Kalvari, Tony Wildish, Gianni Dalla Torre, C.D. Tiwari, David Yuan

Major points:
1. David performed live demo of applications K8S cluster and NFS server with volume with RFAM docker running on the master node.
2. The demo confirmed that two major blockers: “shared fie system for read and write” and “persistent volume for user data” outlined in https://gitlab.ebi.ac.uk/davidyuan/tsi-ccdoc/wikis/rfam-cloud/requirements-and-status-2018.10.09 can be resolved with the applications.

Action items:
1. David to investigate whether Docker container can run as a nonroot user.
2. David to provide assistance for Ioanna to use Cloud Portal.
3. Ioanna to run https://github.com/EMBL-EBI-TSI/cpa-kubernetes and https://github.com/EMBL-EBI-TSI/cpa-nfs-server-and-vol on https://cloud-portal.ebi.ac.uk/.
4. Ioanna to get familiar with Graphana for basic monitoring https://extcloud06.ebi.ac.uk:3000/. Ioanna can deploy https://github.com/EMBL-EBI-TSI/cpa-monitoring on https://cloud-portal.ebi.ac.uk/ for more fine grained monitoring.
5. Ioanna to provide several test cases to verify RFAM pipeline from functional and performance perspectives.
6. David to investigate how to integrate test cases into Jenkins pipeline, as well as invoking cloud portal CLI to deploy RFAM pipeline.